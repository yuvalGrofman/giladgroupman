/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

/**
 * Add your docs here.
 */

public class RobotMap {
    public static final int forward = 1;
    public static final int backward = 0;
    public static final int SOLONOID_1 = 420;
    public static final int SOLONOID_2 = 69;
    public static final int MASTER_FX = 42;
    public static final int SLAVE_FX = 254;
    public static final int conveyorMotor = 10;
    public static final int conveyorAnalogInput = 11;

}
