/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import com.ctre.phoenix.motorcontrol.ControlMode;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.RobotContainer;

public class ConveyorByTime extends CommandBase {

  private double time; 
  private double startTime;
  private double velocity;
  /**
   * Creates a new ConveyorCommand.
   */
  public ConveyorByTime(double time, double velocity) {
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(RobotContainer.conveyer);
    this.time = time;
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    RobotContainer.conveyer.talonFXMotor.set(ControlMode.PercentOutput, 0);
    this.startTime = Timer.getFPGATimestamp();
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    RobotContainer.conveyer.talonFXMotor.set(ControlMode.PercentOutput, velocity);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) { 
    RobotContainer.conveyer.talonFXMotor.set(ControlMode.PercentOutput, 0);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
      if (this.startTime + time == Timer.getFPGATimestamp()){
        return true;
      }
      return false;
  }
}
