/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import frc.robot.Robot;
import frc.robot.RobotContainer;
import frc.robot.RobotMap;

// NOTE:  Consider using this command inline, rather than writing a subclass.  For more
// information, see:
// https://docs.wpilib.org/en/latest/docs/software/commandbased/convenience-features.html
public class ConveyerAnalogWatch extends InstantCommand {
  public ConveyerAnalogWatch() {
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(RobotContainer.conveyer);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {

    if (Robot.ballsIn < 5) {
      if (RobotContainer.conveyer.didBallEnter()) {

        Robot.ballsIn++; 

        Command conveyerByTime = new ConveyorByTime(10.1, 0.7);
        conveyerByTime.schedule();
      }
    }else {

      //Intake.close()
    }
  }
}
