/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.TalonFX;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.RobotMap;

public class Shooter extends SubsystemBase {
  /**
   * Creates a new Shooter.
   */
  public DoubleSolenoid doubleSolenoid1;
  public DoubleSolenoid doubleSolenoid2;
  public TalonFX masterTalonFX;
  public TalonFX slaveTalonFX;


  public Shooter() {
    doubleSolenoid1 = new DoubleSolenoid(RobotMap.SOLONOID_1, RobotMap.forward, RobotMap.backward);
    doubleSolenoid2 = new DoubleSolenoid(RobotMap.SOLONOID_2, RobotMap.forward, RobotMap.backward);
    slaveTalonFX = new TalonFX(RobotMap.SLAVE_FX);
    masterTalonFX = new TalonFX(RobotMap.MASTER_FX);
    slaveTalonFX.set(ControlMode.Follower, masterTalonFX.getDeviceID());
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }
}
