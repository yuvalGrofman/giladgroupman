/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.TalonFX;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;

import edu.wpi.first.wpilibj.AnalogInput;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.RobotMap;

public class Conveyer extends SubsystemBase {

  public WPI_TalonFX talonFXMotor;
  public AnalogInput analogInput; 

  /**
   * Creates a new Conveyer.
   */
  public Conveyer() {
    talonFXMotor = new WPI_TalonFX(RobotMap.conveyorMotor);
    analogInput = new AnalogInput(RobotMap.conveyorAnalogInput);
  }

  public void setPower(Double power){
    talonFXMotor.set(ControlMode.PercentOutput, power); 
  }

  public double getIrValue(){
    return analogInput.getAverageVoltage();
  }

  public boolean didBallEnter(){
    return getIrValue() >= 0.5;
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }
}
